package exception;


public class ComunitarioException extends RuntimeException{

	private static final long serialVersionUID = 4928599035264976611L;
	
	public ComunitarioException(String message) {
		super(message);
	}
	
}
