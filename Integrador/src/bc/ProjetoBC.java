package bc;

import java.util.List;

import exception.ComunitarioException;
import model.Projeto;

public class ProjetoBC implements ComunitarioBC<Projeto>{

	@Override
	public void insert(Projeto t) {
		
	}

	@Override
	public void delete(Projeto t) {

		
	}

	@Override
	public void update(Projeto t) {
		
	}

	@Override
	public Projeto findById(Projeto t) {
		return null;
	}

	@Override
	public List<Projeto> findAll(Projeto t) {
		return null;
	}

	@Override
	public void validateForDataModification(Projeto projeto) {
		
		if(projeto == null){
			throw new ComunitarioException("Projeto Nulo");
		}

		if(projeto.getCargaHoraria()== null){
			throw new ComunitarioException("Carga Horario Nula");
		}
		
		if(projeto.getDescricao()==null){
			throw new ComunitarioException("Descricao Nula");
		}
		
		if(projeto.getInicioProjeto()== null){
			throw new ComunitarioException("Inicio do Projeto Nulo");
		}
		
		
		if(projeto.getInicioProjeto().before(new java.util.Date())){
			throw new ComunitarioException("Inicio do Projeto Antes Data Atual");
		}
		
		if(projeto.getFimProjeto()== null){
			throw new ComunitarioException("Fim do Projeto Nulo");
		}
		
		if(projeto.getFimProjeto().after(projeto.getInicioProjeto())){
			throw new ComunitarioException("Fim do Projeto Antes do Inicio do Projeto");
		}
		
		if(projeto.getPrazoCadastramento()== null){
			throw new ComunitarioException("Prazo de Cadastramneto Nulo");
		}
		
		if(projeto.getPrazoCadastramento().before(projeto.getFimProjeto())){
			throw new ComunitarioException("Prazo de Cadastramneto Depois do Fim do Projeto");
		}
		
		if(projeto.getHorarioProjeto()== null){
			throw new ComunitarioException("Horario do Projeto eh nulo");
		}
		
		if(projeto.getInstituicao()==null){
			throw new ComunitarioException("Instituicao nula");
			
		}
		
		if(projeto.getQtdeVagas()==null){
			throw new ComunitarioException("Qtde de Vagas Nula");
		}
		
		
			
		
	}
	

}
