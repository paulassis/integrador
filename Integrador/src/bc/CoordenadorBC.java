package bc;

import java.util.List;

import exception.ComunitarioException;
import model.Coordenador;

public class CoordenadorBC implements ComunitarioBC<Coordenador>{

	@Override
	public void insert(Coordenador t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Coordenador t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Coordenador t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Coordenador findById(Coordenador t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Coordenador> findAll(Coordenador t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void validateForDataModification(Coordenador coordenador) {
		if(coordenador == null){
			throw new ComunitarioException("Coordenador Nulo");
		}

		
		if(coordenador.getFuncao()==null){
			throw new ComunitarioException("Funcao Nula");
		}
		
		if(coordenador.getAreaAtuacao()== null){
			throw new ComunitarioException("Area de Atuacao Nula");
		}
		
	}

}
