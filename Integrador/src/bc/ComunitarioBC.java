package bc;

import java.util.List;

public interface ComunitarioBC<T> {
	
	public void insert(T t);
	public void delete(T t);
	public void update(T t);
	public T findById(T t);
	public List<T> findAll(T t);
	public void validateForDataModification(T t);


}
