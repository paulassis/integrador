package bc;

import java.util.List;

import exception.ComunitarioException;
import model.Instituicao;

public class InstituicaoBC implements ComunitarioBC<Instituicao> {

	@Override
	public void insert(Instituicao t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Instituicao t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Instituicao t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Instituicao findById(Instituicao t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Instituicao> findAll(Instituicao t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void validateForDataModification(Instituicao instituicao) {
		
		if(instituicao == null){
			throw new ComunitarioException("Projeto Nulo");
		}

		if(instituicao.getEmail()== null){
			throw new ComunitarioException("Email Nula");
		}
		
		if(instituicao.getEndereco()==null){
			throw new ComunitarioException("Endereco Nulo");
		}
		
		if(instituicao.getNome()== null){
			throw new ComunitarioException("Nome Nulo");
		}
		
			
		
	}

}
