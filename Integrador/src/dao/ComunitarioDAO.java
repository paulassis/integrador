package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public abstract class ComunitarioDAO<E> {
		
	
		public abstract long insert(E object);
		public abstract boolean update(E object);
		public abstract boolean delete(E object);
		public abstract E findById(long id);
		public abstract List<E> findAll();
		protected Long retrievePrimaryKeygenerated(ResultSet rs, PreparedStatement ps) throws SQLException{
			//Obtem o ID gerado pelo banco HSQLDB
			rs = ps.getGeneratedKeys();
			if(rs.next()){
				return rs.getLong(1);
			}
			return null;
		}
		protected abstract E populateObject(ResultSet rs) throws SQLException;

}
