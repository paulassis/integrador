package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import model.Projeto;

public class ProjetoDAO extends ComunitarioDAO<Projeto>{

	@Override
	public long insert(Projeto object) {
		return 0;
	}

	@Override
	public boolean update(Projeto object) {
		return false;
	}

	@Override
	public boolean delete(Projeto object) {
		return false;
	}

	@Override
	public Projeto findById(long id) {
		return null;
	}

	@Override
	public List<Projeto> findAll() {
		return null;
	}

	@Override
	protected Projeto populateObject(ResultSet rs) throws SQLException {
		return null;
	}

}
