package model;

import java.util.ArrayList;
import java.util.Date;

public class Projeto {
	private String descricao;
	private String cargaHoraria;
	private Date inicioProjeto;
	private Date fimProjeto;
	private Date prazoCadastramento;
	private String horarioProjeto;
	private ArrayList<Coordenador> coordenadores;
	private Instituicao instituicao;
	private Integer qtdeVagas;
	private boolean Ativo;
	private boolean VagasDisponiveis;
	
	public Projeto(){
		
		this.coordenadores = new ArrayList<Coordenador>();
	}
	

	
	
	public boolean isAtivo() {
		return Ativo;
	}




	public void setAtivo(boolean ativo) {
		this.Ativo = ativo;
	}




	public boolean isVagasDisponiveis() {
		return VagasDisponiveis;
	}




	public void setVagasDisponiveis(boolean vagas) {
		this.VagasDisponiveis = vagas;
	}




	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(String cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public Date getInicioProjeto() {
		return inicioProjeto;
	}
	public void setInicioProjeto(Date inicioProjeto) {
		this.inicioProjeto = inicioProjeto;
	}
	public Date getFimProjeto() {
		return fimProjeto;
	}
	public void setFimProjeto(Date fimProjeto) {
		this.fimProjeto = fimProjeto;
	}
	public Date getPrazoCadastramento() {
		return prazoCadastramento;
	}
	public void setPrazoCadastramento(Date prazoCadastramento) {
		this.prazoCadastramento = prazoCadastramento;
	}
	public String getHorarioProjeto() {
		return horarioProjeto;
	}
	public void setHorarioProjeto(String horarioProjeto) {
		this.horarioProjeto = horarioProjeto;
	}
	public ArrayList<Coordenador> getCoordenadores() {
		return coordenadores;
	}
	public void setCoordenadores(ArrayList<Coordenador> coordenadores) {
		this.coordenadores = coordenadores;
	}
	public Instituicao getInstituicao() {
		return instituicao;
	}
	public void setInstituicao(Instituicao instituicao) {
		this.instituicao = instituicao;
	}
	public Integer getQtdeVagas() {
		return qtdeVagas;
	}
	public void setQtdeVagas(Integer qtdeVagas) {
		this.qtdeVagas = qtdeVagas;
	}
	public ArrayList<String> getPreRequisitos() {
		return preRequisitos;
	}
	public void setPreRequisitos(ArrayList<String> preRequisitos) {
		this.preRequisitos = preRequisitos;
	}
	private ArrayList<String> preRequisitos;
	
	
	
	
}
